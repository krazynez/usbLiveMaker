#!/bin/sh 




clear


function create() {
    clear
    printf '\n\n';
    echo 'You are about to format a drive please make sure you want to do this!'
    echo 'IF YOU FORMATE THE WRONG DRIVE YOU WILL LOOSE EVERYTHING AND IT IS NOT MY FAULT'
    printf '\n';
    read -p 'Do you wish to continue? write "yes": ' make_sure

    if [ $make_sure == 'yes' ]; then
	
	clear
	printf '\n';
	lsblk | grep disk | awk '{ print "Disk:", $1, "Size:", $4, "\n\n"}'
	
	read -p 'Please write which drive you want to create live usb on: ' drive
	
	echo ' '

	read -p 'Please write the path and filename of the iso you want flashed: ' path_file


	read -p 'Are you sure you want drive /dev/'$drive'? y/n: ' ask

	if [ $ask == 'y' ]; then
	    
	    read -p 'Are you sure you want the iso '$path_file'? y/n: ' ask_again

	    if [ $ask_again == 'y' ]; then
		clear
		echo 'Please wait... Program will go back to main menu when done'
		sudo dd if=$path_file of=/dev/$drive bs=4M status=progress && sync
		sudo $SHELL runme.sh
	    else
	    $SHELL runme.sh
	    fi

	else 
	    $SHELL runme.sh
	fi

    else 
	echo 'Going back'
	sleep 1;
	$SHELL runme.sh
	exit 0;
    fi
}


function format() {
    clear
    printf '\n\n';
    echo 'You are about to format a drive please make sure you want to do this!'
    echo 'IF YOU FORMATE THE WRONG DRIVE YOU WILL LOOSE EVERYTHING AND IT IS NOT MY FAULT'
    printf '\n';
    read -p 'Do you wish to continue? write "yes": ' make_sure

    if [ $make_sure == 'yes' ]; then
	
	clear
	printf '\n';
	lsblk | grep disk | awk '{ print "Disk:", $1, "Size:", $4, "\n\n"}'
	
	read -p 'Please write which drive you want to format: ' drive

	echo ' '
	read -p 'Are you sure you want to format /dev/'$drive'? y/n: ' ask

	if [ $ask == 'y' ]; then
	    echo 'Formatting...'
    	    sudo dd if=/dev/zero of=/dev/$drive && sync
	    $SHELL runme.sh
	else 
	    $SHELL runme.sh
	fi

    else 
	echo 'Going back'
	sleep 1;
	$SHELL runme.sh
	exit 0;
    fi

}

function list() {
    clear
    printf '\n';
    lsblk | grep disk | awk '{ print "Disk:", $1, "Size:", $4, "\n\n"}'
    read -p 'Press any key to continue';
    $SHELL runme.sh

}

function x() {
    clear
    printf '\n\n';
    echo "Exiting..."
    sleep 2;
    exit 0;
}


if [[ $EUID -ne 0 ]]; then
    clear
    printf '\n';
    echo 'Please run as root'
    sleep 2;
    exit 1;
fi


echo '#######################################################'
echo '#                                                     #'
echo '#      1.) Create Live USB         2.) Format USB     #'
echo '#                                                     #'
echo '#      3.) List Drives             4.) Exit           #'
echo '#                                                     #'
echo '#######################################################'
echo ''
read -p 'Please Enter a number: ' userInput



case $userInput in
    1)
	create ;;
    2)
	format ;;
    3)
	list ;;
    4)
	x ;;
    *)
	echo "Please enter a number 1-4"
	;;
	
esac













exit 0;
